## Ejercicio - Remplazar vocales

Define el método `replace_vowels` que reciba una lista de palabras y sustituye todas las vocales de cada string con una `"x"`.

```ruby
#replace_vowels method


#Driver code

p replace_vowels(["banana", "apple"]) == ["bxnxnx", "xpplx"]
```